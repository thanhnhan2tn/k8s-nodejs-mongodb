//app.js
const express = require('express');
const bodyParser = require('body-parser');

const product = require('./routes/product.route'); // Imports routes for the products
const db = require('./database/index'); // Imports routes for the database

const app = express();

app.use('/products', product);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

let port = 3000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});


