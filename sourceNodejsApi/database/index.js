

const mongoose = require('mongoose');
let dev_db_url = 'mongodb://someuser:abcd1234@_host_database_name_:_db_port_/_database_name_';
let mongoDB = process.env.MONGODB_URI || dev_db_url;

mongoose.connect(mongoDB,{ useNewUrlParser: true , useUnifiedTopology: true });
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
