
output "my_ami_mongo_public_ip" {
  value = "${module.ami-mongo.my-public-ipaddr}"
}

output "my_ami_minik8s_public_ip" {
  value = "${module.ami-k8s.my-public-ipaddr}"
}
