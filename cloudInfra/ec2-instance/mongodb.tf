
module "sg-mongo" {

   source         = "../modules/createSG"

   _vpc_id_       = "vpc-7487801c"
   _sg_name_      = "mongo-sg"
   _tcp_port_     = "27017" 

}

module "ami-mongo" {

   source             =  "../modules/ubuntu/createAmi"

   _tag_name_         =  "mongodb-devops"
   _ssh_key_name_     =  "devops-ssh-key"
   _sg-acl-id_        =   "ssh-sg"
   _user_             =   "ubuntu"
   _private_key_path_ =   "<< PATH OF YOUR SSH PRIVATE KEY >>"
   _tag_ansible_      =   "mongodb"
   _working_dir_      =   "../../cloudManager"
}

resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = "${module.sg-mongo.id}"
  network_interface_id = "${module.ami-mongo.my-private-ipaddr}"
}
