
variable "_instance_type_" {
  type = "string"
  description = "Type of instance"
  default = "t2.micro"
}

variable "_ssh_key_name_" {
  type = "string"
  description = "SSH Public Key Name"
}

variable "_private_key_path_" {
  type = "string"
  description = "SSH Private Key Path"
}

variable "_user_" {
  type = "string"
  description = "User that will connect with the instance"
}

variable "_sg-acl-id_" {
  type = "string"
  description = "Security Group ACL ID"
}

variable "_tag_name_" {
  type = "string"
  description = "Name of the instance"
}

variable "_tag_ansible_" {
  type = "string"
  description = "Name of the role that will be execute"
}

variable "_working_dir_" {
  type = "string"
  description = "Directory Path of the ansible roles that will be execute"
}
