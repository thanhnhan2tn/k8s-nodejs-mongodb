
output "my-public-ipaddr" {
 value = "${aws_instance.ami-instance.*.public_ip }"
}

output "my-private-ipaddr" {
 value = "${aws_instance.ami-instance.primary_network_interface_id }"
}
