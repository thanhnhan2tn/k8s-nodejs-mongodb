
variable "_ssh_key_name_" {
  type = "string"
  description = "Name of ssh key"
  default = "devops"
}

variable "_ssh_public_key_" {
  type = "string"
  description = "SSH public key"
}
